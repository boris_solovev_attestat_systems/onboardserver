class api {
    constructor(connection) {
        this.connection = connection;
    }

    apiCallStub(interfaceName, methodName, args) {
        return new Promise((resolve, reject) => {
            this.connection.call(interfaceName, methodName, args,
            (args) => {
                resolve(args);
            },
            (args) => {
                reject(args);
            });
        });
    }

    vehicle() {
        return {
            get: (id) => {
                return this.apiCallStub('vehicle', 'get', {id});
            },
            getList: () => {
                return this.apiCallStub('vehicle', 'getList', {});
            },
            add: (vehicle) => {
                return this.apiCallStub('vehicle', 'add', { 
                    type: vehicle.type, 
                    registration_number: vehicle.registration_number,
                    route_number: vehicle.route_number,
                    contact: vehicle.contact,
                    description: vehicle.description
                });
            },
            update: (vehicle) => {
                return this.apiCallStub('vehicle', 'update', {
                    id: vehicle.id, 
                    type: vehicle.type, 
                    registration_number: vehicle.registration_number,
                    route_number: vehicle.route_number,
                    contact: vehicle.contact,
                    description: vehicle.description
                });
            },
            remove: (id) => {
                return this.apiCallStub('vehicle', 'remove', {id});
            },
        }
    }

    terminal() {
        return {
            get: (id) => {
                return this.apiCallStub('terminal', 'get', {id});
            },
            getState: (id) => {
                return this.apiCallStub('terminal', 'getState', {id});
            },
            getList: () => {
                return this.apiCallStub('terminal', 'getList', {});
            },
            getListByVehicleId: (vehicle_id) => {
                return this.apiCallStub('terminal', 'getListByVehicle', {vehicle_id});
            },
            add: (terminal) => {
                return this.apiCallStub('terminal', 'add', {
                    serial: terminal.serial, 
                    description: terminal.description, 
                    vehicle_id: terminal.vehicle_id, 
                    model: terminal.model,
                    software: terminal.software
                });
            },
            update: (terminal) => {
                return this.apiCallStub('terminal', 'update', {
                    id: terminal.id,
                    serial: terminal.serial, 
                    description: terminal.description, 
                    vehicle_id: terminal.vehicle_id, 
                    model: terminal.model,
                    software: terminal.software
                });
            },
            remove: (id) => {
                return this.apiCallStub('terminal', 'remove', {id});
            },
            getState: (id) => {
                return this.apiCallStub('terminal', 'getState', {id});
            },
            getData: (channel_id, value, startDate, stopDate) => {
                return this.apiCallStub('terminal', 'getData', {channel_id, value, startDate, stopDate});
            }
        }
    }

    channel() {
        return {
            get: (id) => {
                return this.apiCallStub('terminal', 'getChannel', {id});
            },
            getList: (terminal_id) => {
                return this.apiCallStub('terminal', 'getChannelList', {terminal_id});
            },
            add: (channel) => {
                return this.apiCallStub('terminal', 'addChannel', {
                    terminal_id: channel.terminal_id,
                    serial: channel.serial,
                    door: channel.door,
                    description: channel.description
                });
            },
            update: (channel) => {
                return this.apiCallStub('terminal', 'updateChannel', {
                    id: channel.id,
                    serial: channel.serial,
                    door: channel.door,
                    description: channel.description
                });
            },
            remove: (id) => {
                return this.apiCallStub('terminal', 'removeChannel', {id});
            },
            getState: (id) => {
                return this.apiCallStub('terminal', 'getChannelState', {id});
            },
        }
    }

    control() {
        return {
            installSoftware: (terminal_id, link) => {
                return this.apiCallStub('control', 'installSoftware', {terminal_id, link});
            },
            subscribe: (terminal_id) => {
                return this.apiCallStub('control', 'subscribe', {terminal_id});
            },
            unsubscribe: (terminal_id) => {
                return this.apiCallStub('control', 'unsubscribe', {terminal_id});
            },
            unsubscribeAll: () => {
                return this.apiCallStub('control', 'unsubscribeAll', {});
            },
            subscribeStream: (terminal_id, index, type) => {
                return this.apiCallStub('control', 'subscribeStream', {terminal_id, index, type});
            },
            unsubscribeStream: (terminal_id, index, type) => {
                return this.apiCallStub('control', 'unsubscribeStream', {terminal_id, index, type});
            },
            resetCounters: (terminal_id) => {
                return this.apiCallStub('control', 'resetCounters', {terminal_id});
            },

            readInfo: () => {
                return this.apiCallStub('control', 'readInfo', {});
            },

            setTerminalValue: (terminal_id, valueName, value) => {
                return this.apiCallStub('control', 'setTerminalValue', {terminal_id, valueName, value});
            },

            setChannelValue: (terminal_id, channelIndex, valueName, value) => {
                return this.apiCallStub('control', 'setChannelValue', {terminal_id, index: channelIndex, valueName, value});
            }
        }
    }
}
