var express = require('express');
var app = express();
var path = require('path');

app.use('/public', express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

var server = app.listen(9001, function(){
  console.log('Server listening on port ' + 9001);
});

module.exports = server;